DESCRIPTION:
------------
Search Synonyms provides an interface for defining terms and their synonyms
which are used to improve search capabilities by converting search terms that are
acronyms or abbreviations that are not commonly known. This improves usability
on niche sites that need flexible searches to ensure content is properly located.

INSTALLATION:
-------------
1. Extract the tar.gz into your 'modules' or directory.
2. Enable the module at 'administer >> site building >> modules'.

CONFIGURATION
--------------
1. Visit 'administer >> settings >> search synonyms'

INSTRUCTIONS
--------------
1. Navigate to the settings page at admin/settings/searchsynonyms.
2. To add a term, click the "Add term" tab.
    - Terms are the word(s) that synonyms will be converted to while 
      searches are executed.
    - Terms can have letters, dashes, and spaces.
3. To add a synonym to a term, click the "add synonyms" link
    next to the term of your choice.
    - Each term can have multiple synonyms.
    - Synonyms can have letters, dashes, and spaces.
4. To view synonyms associated with a specific term, click 
    "list synonyms" next to the desired term while viewing the term list.
5. To delete a term or synonym, edit the item and click delete.
6. When users search your site, synonyms that are located within
    the search text will be replaced by their corresponding term.
    - Only full words will be replaced with their corresponding term,
      so if you have a term "woods" with the synonym "tiger" then only
      the word "tiger" will be replaced with "woods" in a search.
      If a user searches for "tigerlilly" it will not be converted.

UNINSTALLTION:
--------------
1. Disable the module.
2. Uninstall the module; database tables will be removed.

