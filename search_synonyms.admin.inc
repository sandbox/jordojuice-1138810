<?php

/**
 * @file
 * Administrative page callbacks for the search_synonyms module.
 */

/**
 * Form builder to list and magage terms and their synonyms.
 *
 * @ingroup forms
 */

function search_synonyms_overview_syn_terms() {
  $search_synonyms_syn_terms = search_synonyms_get_syn_terms();
  foreach ($search_synonyms_syn_terms as $syn_term) {
    $form[$syn_term->ss_tid]['#syn_term'] = (array)$syn_term;
    $form[$syn_term->ss_tid]['syn_term'] = array('#value' => check_plain($syn_term->syn_term));
    $form[$syn_term->ss_tid]['description'] = array('#value' => check_plain($syn_term->description));
    $form[$syn_term->ss_tid]['edit'] = array('#value' => l(t('edit term'), "admin/settings/searchsynonyms/edit/term/$syn_term->ss_tid"));
    $form[$syn_term->ss_tid]['list'] = array('#value' => l(t('list synonyms'), "admin/settings/searchsynonyms/$syn_term->ss_tid"));
    $form[$syn_term->ss_tid]['add'] = array('#value' => l(t('add synonyms'), "admin/settings/searchsynonyms/$syn_term->ss_tid/add/synonym"));
  }

  return $form;
}

/**
 * Theming the terms overview page as a sortable list
 *
 * @ingroup themable
 * @see search_synonyms_overview_syn_terms()
 */
function theme_search_synonyms_overview_syn_terms($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['syn_term'])) {
      $syn_term = &$form[$key];

      $row = array();
      $row[] = drupal_render($syn_term['syn_term']);
      $row[] = drupal_render($syn_term['description']);
      $row[] = drupal_render($syn_term['edit']);
      $row[] = drupal_render($syn_term['list']);
      $row[] = drupal_render($syn_term['add']);
      $rows[] = array('data' => $row);
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No terms available.'), 'colspan' => '5'));
  }

  $header = array(t('Term'));
  $header[] = t('Description');

  $header[] = array('data' => t('Operations'), 'colspan' => '3');
  return theme('table', $header, $rows, array('id' => 'search_syn_terms')) . drupal_render($form);
}
  
/**
 * Display form for adding and editing terms.
 *
 * @ingroup forms
 * @see search_synonyms_form_syn_term_submit()
 */
function search_synonyms_form_syn_term(&$form_state, $edit = array()) {
  $edit += array(
    'syn_term' => '',
    'description' => '',
  );
  $form['identification'] = array(
    '#type' => 'fieldset',
    '#title' => t('Identification'),
    '#collapsible' => TRUE,
  );
  $form['identification']['syn_term'] = array(
    '#type' => 'textfield',
    '#title' => t('Term'),
    '#default_value' => $edit['syn_term'],
    '#maxlength' => 255,
    '#description' => t('The term you would like acronyms to be converted to.'),
    '#required' => TRUE,
  );
  $form['identification']['description'] = array(
    '#type' => 'textarea',
    '#default_value' => $edit['description'],
    '#description' => t('Description of the term for administrative purposes.'),
  );
  
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  if (isset($edit['ss_tid'])) {
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
    $form['ss_tid'] = array('#type' => 'value', '#value' => $edit['ss_tid']);
  }
  return $form;
}

/**
 * Validation handler for the term edit form. Ensure the term
 * does not contain inappropriate characters.
 *
 * @see search_synonyms_form_syn_term()
 */
function search_synonyms_form_syn_term_validate($form, &$form_state) {
  if (isset($form_state['values']['syn_term']) && is_numeric($form_state['values']['syn_term'])) {
    form_set_error('syn_term', t('Term value cannot be numeric.'));
  }
  elseif (isset($form_state['values']['syn_term']) & !eregi("^[a-zA-Z -]{2,40}$", $form_state['values']['syn_term'])) {
    form_set_error('syn_term', t('The term contains an unacceptable value.'));
  }
}

/**
 * Accept the form submissions for a term and save the results.
 */
function search_synonyms_form_syn_term_submit($form, &$form_state) {
  switch (search_synonyms_save_syn_term($form_state['values'])) {
    case SAVED_NEW:
      drupal_set_message(t('Created new term %syn_term.', array('%syn_term' => $form_state['values']['syn_term'])));
      watchdog('search_synonyms', 'Created new term %syn_term.', array('%syn_term' => $form_state['values']['syn_term']), WATCHDOG_NOTICE, l(t('edit'), 'admin/settings/searchsynonyms/edit/term/'. $form_state['values']['ss_tid']));
      break;
    case SAVED_UPDATED:
      drupal_set_message(t('Updated term %syn_term.', array('%syn_term' => $form_state['value']['syn_term'])));
      watchdog('search_synonyms', 'Updated term %syn_term.', array('%syn_term' => $form_state['value']['syn_term']), WATCHDOG_NOTICE, l(t('edit'), 'admin/settings/searchsynonyms/edit/term/'. $form_state['value']['ss_tid']));
      break;
  }
  
  $form_state['ss_tid'] = $form_state['values']['ss_tid'];
  $form_state['redirect'] = 'admin/settings/searchsynonyms';
  return;
}
  
/**
 * Page to edit a term.
 *
 * @param $syn_term
 *   A term object.
 */
function search_synonyms_admin_syn_term_edit($syn_term) {
  if ((isset($_POST['op']) && $_POST['op'] == t('Delete')) || isset($_POST['confirm'])) {
    return drupal_get_form('search_synonyms_syn_term_confirm_delete', $syn_term->ss_tid);
  }
  return drupal_get_form('search_synonyms_form_syn_term', (array)$syn_term);
}

/**
 * Page to edit a synonym.
 *
 * @param $ss_sid
 *   The id of a synonym.
 */
function search_synonyms_admin_synonym_edit($ss_sid) {
  if ($synonym = (array)search_synonyms_get_synonym($ss_sid)) {
    return drupal_get_form('search_synonyms_form_synonym', search_synonyms_syn_term_load($synonym['ss_tid']), $synonym);
  }
  return drupal_not_found();
}

/**
 * Display all synonyms assigned to a given term.
 *
 * @ingroup forms
 * @see search_synonyms_overview_synonyms_submit(), theme_search_synonyms_overview_synonyms()
 */
function search_synonyms_overview_synonyms(&$form_state, $syn_term) {
  drupal_set_title(t('Synonyms in %syn_term', array('%syn_term' => $syn_term->syn_term)));
  $form = array(
    '#syn_term' => (array)$syn_term,
  );
  
  $search_synonyms_synonyms = search_synonyms_get_synonyms($syn_term->ss_tid);
  if ($search_synonyms_synonyms) {
    foreach ($search_synonyms_synonyms as $synonym) {
      $form[$synonym->ss_sid]['#synonym'] = (array)$synonym;
      $form[$synonym->ss_sid]['synonym'] = array('#value' => check_plain($synonym->synonym));
      $form[$synonym->ss_sid]['description'] = array('#value' => check_plain($synonym->description));
      $form[$synonym->ss_sid]['edit'] = array('#value' => l(t('edit synonym'), "admin/settings/searchsynonyms/edit/synonym/$synonym->ss_sid"));
    }
  }

  return $form;
}

/**
 * Theming the synonyms overview page as a sortable list
 *
 * @ingroup themable
 * @see search_synonyms_overview_synonyms()
 */
function theme_search_synonyms_overview_synonyms($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['synonym'])) {
      $synonym = &$form[$key];
      
      $row = array();
      $row[] = drupal_render($synonym['synonym']);
      $row[] = drupal_render($synonym['description']);
      $row[] = drupal_render($synonym['edit']);
      $row[] = drupal_render($synonym['delete']);
      $rows[] = array('data' => $row);
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No synonyms available.'), 'colspan' => '4'));
  }
  
  $header = array(t('Synonym'));
  $header[] = t('Description');
  
  $header[] = array('data' => t('Operations'), 'colspan' => '2');
  return theme('table', $header, $rows, array('id' => 'search_synonyms')) . drupal_render($form);
}

  
/**
 * Menu callback; return the edit form for a new synonym after setting the title.
 *
 * @param $syn_term
 *   A term object.
 */
function search_synonyms_add_synonym_page($syn_term) {
  drupal_set_title(t('Add synonym to %search_synonyms_syn_term', array('%search_synonyms_syn_term' => $syn_term->syn_term)));
  return drupal_get_form('search_synonyms_form_synonym' , $syn_term);
}

/**
 * Form function for the synonym edit form.
 *
 * @ingroup forms
 * @see search_synonyms_form_synonym_submit()
 */
function search_synonyms_form_synonym(&$form_state, $syn_term, $edit = array()) {
  $edit += array(
    'synonym' => '',
    'description' => '',
    'ss_sid' => NULL,
  );

  $form['#synonym'] = $edit;
  $form['#syn_term'] = (array)$syn_term;

  // Check for confirmation forms.
  if (isset($form_state['confirm_delete'])) {
    return array_merge($form, search_synonyms_synonym_confirm_delete($form_state, $edit['ss_sid']));
  };

  $form['identification'] = array(
    '#type' => 'fieldset',
    '#title' => t('Identification'),
    '#collapsible' => TRUE,
  );
  $form['identification']['synonym'] = array(
    '#type' => 'textfield',
    '#title' => t('Synonym text'),
    '#default_value' => $edit['synonym'],
    '#maxlength' => 40,
    '#description' => t('The text of the synonym.'),
    '#required' => TRUE
  );
  $form['identification']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#maxlength' => 255,
    '#default_value' => $edit['description'],
    '#description' => t('A description of the synonym for administration.')
  );
  $form['ss_tid'] = array(
    '#type' => 'value',
    '#value' => $syn_term->ss_tid
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  if ($edit['ss_sid']) {
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete')
    );
    $form['ss_sid'] = array(
      '#type' => 'value',
      '#value' => $edit['ss_sid']
    );
  }
  else {
    $form['destination'] = array('#type' => 'hidden', '#value' => $_GET['q']);
  }

  return $form;
}

/**
 * Validation handler for the synonym edit form. Ensure the synonym
 * does not contain inappropriate characters.
 *
 * @see search_synonyms_form_synonym()
 */
function search_synonyms_form_synonym_validate($form, &$form_state) {
  if (isset($form_state['values']['synonym']) && is_numeric($form_state['values']['synonym'])) {
    form_set_error('synonym', t('Synonym value cannot be numeric.'));
  }
  elseif (isset($form_state['values']['synonym']) & !eregi("^[a-zA-Z -]{2,40}$", $form_state['values']['synonym'])) {
    form_set_error('synonym', t('The synonym contains an unacceptable value. Synonyms may have only letters, dashes, and spaces.'));
  }
}

/**
 * Submit handler to insert or update a synonym.
 *
 * @see search_synonyms_form_synonym()
 */
function search_synonyms_form_synonym_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Delete')) {
    // Execute the synonym deletion.
    if ($form_state['values']['delete'] === TRUE) {
      return search_synonyms_synonym_confirm_delete_submit($form, $form_state);
    }
    // Rebuild the form to confirm synonym deletion.
    $form_state['rebuild'] = TRUE;
    $form_state['confirm_delete'] = TRUE;
    return;
  }

  switch (search_synonyms_save_synonym($form_state['values'])) {
    case SAVED_NEW:
      drupal_set_message(t('Created new synonym %synonym.', array('%synonym' => $form_state['values']['synonym'])));
      watchdog('search_synonyms', 'Created new synonym %synonym.', array('%synonym' => $form_state['values']['synonym']), WATCHDOG_NOTICE, l(t('edit'), 'admin/settings/searchsynonyms/edit/synonym/'. $form_state['values']['ss_sid']));
      break;
    case SAVED_UPDATED:
      drupal_set_message(t('Updated synonym %synonym.', array('%synonym' => $form_state['values']['synonym'])));
      watchdog('search_synonyms', 'Updated synonym %synonym.', array('%synonym' => $form_state['values']['synonym']), WATCHDOG_NOTICE, l(t('edit'), 'admin/settings/searchsynonyms/edit/synonym/'. $form_state['values']['ss_sid']));
      break;
  }

  $form_state['ss_sid'] = $form_state['values']['ss_sid'];
  $form_state['redirect'] = 'admin/settings/searchsynonyms/' . $form_state['values']['ss_tid'];
  return;
}

/**
 * Form builder for the synonym delete form.
 *
 * @param $ss_sid
 *   The id of a synonym.
 *
 * @ingroup forms
 * @see search_synonyms_synonym_confirm_delete_submit()
 */
function search_synonyms_synonym_confirm_delete(&$form_state, $ss_sid) {
  $synonym = search_synonyms_get_synonym($ss_sid);

  $form['type'] = array('#type' => 'value', '#value' => 'synonym');
  $form['synonym'] = array('#type' => 'value', '#value' => $synonym->synonym);
  $form['ss_sid'] = array('#type' => 'value', '#value' => $ss_sid);
  $form['ss_tid'] = array('#type' => 'value', '#value' => $synonym->ss_tid);
  $form['delete'] = array('#type' => 'value', '#value' => TRUE);
  return confirm_form($form,
                  t('Are you sure you want to delete the synonym %title?',
                  array('%title' => $synonym->synonym)),
                  'admin/settings/searchsynonyms/',
                  t('This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}

/**
 * Submit handler to delete a synonym after confirmation.
 *
 * @see search_synonyms_synonym_confirm_delete()
 */
function search_synonyms_synonym_confirm_delete_submit($form, &$form_state) {
  search_synonyms_del_synonym($form_state['values']['ss_sid']);
  drupal_set_message(t('Deleted synonym %synonym.', array('%synonym' => $form_state['values']['synonym'])));
  watchdog('search_synonyms', 'Deleted synonym %synonym.', array('%synonym' => $form_state['values']['synonym']), WATCHDOG_NOTICE);
  $form_state['redirect'] = 'admin/settings/searchsynonyms/' . $form_state['values']['ss_tid'];
  return;
}

/**
 * Form builder for the term delete confirmation form.
 *
 * @param $ss_tid
 *   The ID of a term.
 *
 * @ingroup forms
 * @see search_synonyms_syn_term_confirm_delete_submit()
 */
function search_synonyms_syn_term_confirm_delete(&$form_state, $ss_tid) {
  $syn_term = search_synonyms_syn_term_load($ss_tid);

  $form['type'] = array('#type' => 'value', '#value' => 'term');
  $form['ss_tid'] = array('#type' => 'value', '#value' => $syn_term->ss_tid);
  $form['syn_term'] = array('#type' => 'value', '#value' => $syn_term->syn_term);
  return confirm_form($form,
                  t('Are you sure you want to delete the term %title?',
                  array('%title' => $syn_term->syn_term)),
                  'admin/settings/searchsynonyms',
                  t('Deleting a term will delete all the synonyms in it. This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}

/**
 * Submit handler to delete a term after confirmation.
 *
 * @see search_synonyms_syn_term_confirm_delete()
 */
function search_synonyms_syn_term_confirm_delete_submit($form, &$form_state) {
  $status = search_synonyms_del_syn_term($form_state['values']['ss_tid']);
  drupal_set_message(t('Deleted term %syn_term.', array('%syn_term' => $form_state['values']['syn_term'])));
  watchdog('search_synonyms', 'Deleted term %syn_term.', array('%syn_term' => $form_state['values']['syn_term']), WATCHDOG_NOTICE);
  $form_state['redirect'] = 'admin/settings/searchsynonyms';
  return;
}
